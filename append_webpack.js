/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                                                                                        
              ██████╗  ██████╗ ██████╗ ████████╗██╗███████╗ ██████╗ ██╗     ██╗ ██████╗ 
              ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██║██╔════╝██╔═══██╗██║     ██║██╔═══██╗
    █████╗    ██████╔╝██║   ██║██████╔╝   ██║   ██║█████╗  ██║   ██║██║     ██║██║   ██║
    ╚════╝    ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██║██╔══╝  ██║   ██║██║     ██║██║   ██║
              ██║     ╚██████╔╝██║  ██║   ██║   ██║██║     ╚██████╔╝███████╗██║╚██████╔╝
              ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝      ╚═════╝ ╚══════╝╚═╝ ╚═════╝ 
                                                                                        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

paths['portifolio'] = {
  'news': src.vendors + 'dataview-intranetone-news/src/',
  'moment': src.vendors + 'moment/',
  'moment_duration_format': src.vendors + 'moment-duration-format/lib/',
  'wickedpicker': src.vendors + 'wickedpicker/dist/',
  'dropzone': src.vendors + 'dropzone/dist/',
  'photoswipe': src.vendors + 'photoswipe/dist/',
  'maskmoney': src.vendors + 'jquery-maskmoney/dist/',
  'autocomplete': 'resources/assets/vendors/devbridge-autocomplete/dist/',
}

mix.styles([
	src.base.css + 'helpers/dv-buttons.css',
	src.io.css + 'dropzone.css',
	src.io.css + 'dropzone-preview-template.css',
  src.io.vendors + 'aanjulena-bs-toggle-switch/aanjulena-bs-toggle-switch.css',
	paths.toastr + 'toastr.min.css',
	src.io.css + 'toastr.css',
  src.io.custom + 'custom-devbridge.css',
  src.io.root + 'forms/video-form.css',
  paths.portifolio.portifolio + 'portifolio.css',
], dest.io.services + 'io-portifolio.min.css');

mix.babel([
  src.io.vendors + 'aanjulena-bs-toggle-switch/aanjulena-bs-toggle-switch.js',
	paths.toastr + 'toastr.min.js',
  paths.portifolio.wickedpicker + 'wickedpicker.min.js',
	src.io.js + 'defaults/def-toastr.js',
  paths.news.dropzone + 'min/dropzone.min.js',
  src.io.js + 'dropzone-loader.js',
  paths.portifolio.maskmoney + 'jquery.maskMoney.min.js',
  paths.portifolio.autocomplete + 'jquery.autocomplete.min.js'
], dest.io.services + 'io-portifolio-babel.min.js');

mix.scripts([
	paths.portifolio.moment + 'min/moment.min.js',
	src.io.vendors + 'moment/moment-pt-br.js',
  paths.portifolio.moment_duration_format +'moment-duration-format.js',
], dest.io.services + 'io-portifolio-mix.min.js');

mix.copy(src.base.js + 'data/cidades_otimizado.js', dest.io.js+'cidades_otimizado.js');  //babel off
mix.babel(paths.portifolio.portifolio + 'portifolio.js', dest.io.services + 'io-portifolio.min.js');
